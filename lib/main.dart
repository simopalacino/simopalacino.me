import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:highlights_page/avatar.dart';
import 'package:highlights_page/buttons.dart';
import 'package:highlights_page/cool_icon_button.dart';
import 'package:highlights_page/who_am_i.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Simone Palacino Page',
      home: Scaffold(
        body: Area(
          bg: AssetImage('assets/images/bg.jpg'),
          child: ContentWidget(),
        ),
      ),
    );
  }
}

class ContentWidget extends StatelessWidget {
  const ContentWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final h = constraints.maxHeight;
      return ListView(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: (h / 2 - 300) - ((h / 2) - 300) / (h * 200 - 300 * 200),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Buttons(list: <ButtonData>[
                    ButtonData(
                      name: 'my professional network',
                      icon: FontAwesomeIcons.linkedin,
                      shadowColor: const Color(0xff0a66c2),
                      link: 'https://linkedin.com/in/simopalacino',
                    ),
                    ButtonData(
                      name: 'download my resume',
                      icon: Icons.menu_book,
                      shadowColor: const Color.fromARGB(255, 105, 240, 240),
                      action: () => _downloadFile('resume.pdf'),
                    ),
                    ButtonData(
                      name: 'my dev.to articles',
                      icon: FontAwesomeIcons.dev,
                      shadowColor: const Color.fromARGB(255, 0, 0, 0),
                      link: 'https://dev.to/simopalacino',
                    ),
                    ButtonData(
                      name: 'my random conding',
                      icon: FontAwesomeIcons.gitlab,
                      shadowColor: const Color.fromARGB(255, 252, 109, 38),
                      link: 'https://gitlab.com/users/simopalacino/projects',
                    ),
                    ButtonData(
                      name: 'email me',
                      icon: Icons.email,
                      shadowColor: Colors.red,
                      link: 'mailto:simo.palacino@gmail.com',
                      action: () => _emailAction(context),
                    ),
                    ButtonData(
                      name: 'my public key (keybase.io/simopalacino)',
                      icon: FontAwesomeIcons.key,
                      shadowColor: const Color(0xff8e80a6),
                      link: 'https://keybase.io/simopalacino',
                    ),
                    ButtonData(
                      name: 'build an e-commerce?',
                      icon: FontAwesomeIcons.cartShopping,
                      shadowColor: Colors.amber,
                      action: () => _showPopup(
                        context,
                        const Text(
                            'If you want to build an eccomerce contact me via email'),
                      ),
                    ),
                    ButtonData(
                      name: 'my paypal',
                      icon: FontAwesomeIcons.paypal,
                      shadowColor: const Color(0xff003087),
                      link: 'https://paypal.me/simopalacino',
                    ),
                    ButtonData(
                      name: 'my amazon wish list',
                      icon: FontAwesomeIcons.amazon,
                      shadowColor: const Color(0xffff9900),
                      link:
                          'https://www.amazon.it/hz/wishlist/ls/ZL14W3FVHGXZ?ref_=wl_share',
                    ),
                    ButtonData(
                      name: 'my twitter',
                      icon: FontAwesomeIcons.twitter,
                      shadowColor: const Color(0xff1d9bf0),
                      link: 'https://twitter.com/simopalacino',
                    ),
                  ]),
                  const Avatar(),
                ],
              ),
              const WhoAmI(),
            ],
          ),
        ],
      );
    });
  }
}

class Area extends StatelessWidget {
  final AssetImage? bg;
  final Widget? child;
  const Area({
    super.key,
    this.bg,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Stack(
        children: [
          OverflowBox(
            minHeight: constraints.maxHeight,
            minWidth: constraints.maxWidth,
            maxWidth: double.infinity,
            maxHeight: double.infinity,
            child: bg == null
                ? Container()
                : Image(
                    image: bg!,
                    fit: BoxFit.cover,
                  ),
          ),
          child ?? Container(),
        ],
      );
    });
  }
}

void _emailAction(BuildContext context) {
  Clipboard.setData(const ClipboardData(text: 'simo.palacino@gmail.com'));
  _showPopup(
    context,
    Tooltip(
      message: 'Click me to open your default e-mail client',
      child: RichText(
        text: TextSpan(
          children: [
            const TextSpan(text: 'E-Mail copied in clipboard! :)\n'),
            TextSpan(
              text: 'simo.palacino@gmail.com',
              style: const TextStyle(decoration: TextDecoration.underline),
              recognizer: TapGestureRecognizer()
                ..onTap = () =>
                    launchUrl(Uri.parse('mailto:simo.palacino@gmail.com')),
            ),
          ],
        ),
      ),
    ),
  );
}

void _showPopup(BuildContext context, Widget child) {
  showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(content: child);
    },
  );

  // Future.delayed(const Duration(seconds: 3), () => Navigator.of(context).pop());
}

void _downloadFile(String fName) {
  launchUrl(Uri(path: 'files/$fName'));
}
