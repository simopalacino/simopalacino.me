import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  static const double size = 288;

  const Avatar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final cSize = size > constraints.maxWidth * 0.48828125
          ? constraints.maxWidth * 0.48828125
          : size;
      return Stack(
        alignment: Alignment.center,
        children: [
          Container(
            constraints: BoxConstraints(
              minHeight: cSize,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.5),
                  blurRadius: 1,
                  spreadRadius: 1,
                ),
              ],
            ),
          ),
          Container(
            constraints: BoxConstraints(minHeight: cSize - cSize * 0.0234375),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage('assets/images/avatar.png'),
              ),
            ),
          ),
        ],
      );
    });
  }
}
