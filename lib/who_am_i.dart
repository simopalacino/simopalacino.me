import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class WhoAmI extends StatelessWidget {
  const WhoAmI({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 512 * 1.1),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: const TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontFamily: 'Awesome',
          ),
          children: [
            const TextSpan(
              text: 'Simone Palacino\n',
              style: TextStyle(fontSize: 44),
            ),
            const TextSpan(
              text: 'Embedded AI Video-Analysis Project Lead & Engineer @ ',
              style: TextStyle(fontSize: 22),
            ),
            TextSpan(
              text: 'Bettini Srl\n',
              style: const TextStyle(
                fontSize: 22,
                decoration: TextDecoration.underline,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  launchUrl(Uri.parse('https://www.bettinivideo.com'));
                },
            ),
            const TextSpan(
                text: 'Sector: CCTV Surveillance / Embedded Devices\n'),
            const TextSpan(text: '📍 Location: Milan, ITALY\n'),
          ],
        ),
      ),
    );
  }
}
