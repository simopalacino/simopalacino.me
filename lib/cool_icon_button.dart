import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ButtonData {
  String name;
  IconData icon;
  Color shadowColor;
  String? link;
  void Function()? action;

  ButtonData({
    required this.name,
    required this.icon,
    required this.shadowColor,
    this.link,
    this.action,
  });
}

class CoolIconButton extends StatefulWidget {
  final ButtonData btnData;

  const CoolIconButton({
    super.key,
    required this.btnData,
  });

  @override
  createState() => _CoolIconButtonState();
}

class _CoolIconButtonState extends State<CoolIconButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _scaleAnimation;

  bool isHovered = false;

  void _handleTap() async {
    if (widget.btnData.action != null) {
      widget.btnData.action!();
      return;
    }

    if (widget.btnData.link != null &&
        !await launchUrl(Uri.parse(widget.btnData.link!))) {
      throw 'Error launching url ${widget.btnData.link}';
    }
  }

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );

    _scaleAnimation = Tween<double>(begin: 1, end: 1.1).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeInOut,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _handleTap,
      onTapDown: (details) => setState(() => isHovered = true),
      onTapUp: (details) => setState(() => isHovered = false),
      onTapCancel: () => setState(() => isHovered = false),
      onHover: (hovered) {
        if (hovered) {
          _controller.forward();
        } else {
          _controller.reverse();
        }
        setState(() => isHovered = hovered);
      },
      child: ScaleTransition(
        scale: _scaleAnimation,
        child: Tooltip(
          message: widget.btnData.name,
          child: Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color:
                  isHovered ? widget.btnData.shadowColor : Colors.transparent,
              boxShadow: isHovered
                  ? [
                      BoxShadow(
                        color: widget.btnData.shadowColor.withOpacity(0.5),
                        blurRadius: 10,
                        spreadRadius: 3,
                      ),
                    ]
                  : [],
            ),
            child: Icon(
              widget.btnData.icon,
              color: isHovered
                  ? Colors.white
                  : const Color.fromARGB(255, 85, 85, 85),
              size: 24,
            ),
          ),
        ),
      ),
    );
  }
}
