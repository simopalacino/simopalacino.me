import 'dart:math';
import 'package:flutter/material.dart';
import 'package:highlights_page/cool_icon_button.dart';

class Buttons extends StatelessWidget {
  final double sizeArea = 422.4;
  final List<ButtonData>? list;
  const Buttons({
    super.key,
    this.list,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final double bord =
          sizeArea < constraints.maxWidth * 0.75 ? 48 : 48 * 0.75;
      final cSize = min(sizeArea, constraints.maxWidth * 0.75 + bord);
      return SizedBox(
        width: cSize + bord,
        height: cSize + bord,
        child: Builder(builder: (context) {
          var children = <Widget>[];
          if (list == null) {
            return Container();
          }

          final listSize = list!.length;
          for (var i = 0; i < listSize; ++i) {
            final ang = 2 * pi / listSize * -i;
            children.add(
              Positioned(
                left: sin(ang) * (cSize / 2) +
                    (cSize / 2) +
                    -sin(ang) * (bord / 2),
                top: cos(ang) * -(cSize / 2) +
                    (cSize / 2) -
                    -cos(ang) * (bord / 2),
                child: SizedBox(
                  width: bord,
                  height: bord,
                  child: CoolIconButton(btnData: list![i]),
                ),
              ),
            );
          }
          return Stack(children: children);
        }),
      );
    });
  }
}
