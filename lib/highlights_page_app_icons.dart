import 'package:flutter/widgets.dart';

class HighlightsPageApp {
  HighlightsPageApp._();

  static const _kFontFam = 'HighlightsPageApp';
  static const String? _kFontPkg = null;

  static const IconData linkedIn = IconData(
    0xe801,
    fontFamily: _kFontFam,
    fontPackage: _kFontPkg,
  );
}
